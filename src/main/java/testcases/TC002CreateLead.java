package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC002";
		
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName, String password, String logInName, String companyname, String firstname, String lastname) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.clickCRMButton()
		.clickLeadButton()
		.clickCreateLeadButton()
		.enterCompanyName(companyname)
		.enterFirstName(firstname)
		.enterLastName(lastname)
		.clickCreatLeadButton()
		.viewFirstName(firstname);
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	

}
