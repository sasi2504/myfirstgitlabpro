package pages;

import wrappers.Annotations;

public class clickFindLeadsButton extends Annotations {
	
	public clickFindLeadsButton editFirstName(String data) {
		driver.findElementByXPath("//input[@name='firstName' and@id='ext-gen248']").sendKeys(data);
		return this;
	}
	public clickFindLeadsButton editLastName(String data) {
		driver.findElementByXPath("//input[@name='lastName' and@id='ext-gen250']").sendKeys(data);
		return this;
	}
	public clickFindLeadsButton editCompanyName(String data) {
		driver.findElementByXPath("//input[@name='companyName' and@id='ext-gen252']").sendKeys(data);
		return this;
	}
	public clickFindLeadsButton clickButtonFindLeads() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	public ViewLeadForEditInfo viewGivenInfo() {
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		return new ViewLeadForEditInfo();
		
	}
	
	
	
	
	
	
	

}
