package pages;

import org.openqa.selenium.WebElement;


public class MyHomePage extends Anno {

	public MyLeadPage clickLeadButton() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeadPage();
	}
	
}

