package pages;

import wrappers.Annotations;

public class MyLeadPage extends Annotations {
	
	public MyCreateLeadPage clickCreateLeadButton() {
		driver.findElementByLinkText("Create Lead").click();
		return new MyCreateLeadPage();
		
	}

}
