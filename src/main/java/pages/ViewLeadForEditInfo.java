package pages;

import wrappers.Annotations;

public class ViewLeadForEditInfo extends Annotations {
	public ViewLeadForEditInfo clickEditButton() {
		driver.findElementByLinkText("Edit").click();
		return this;
	}
	public ViewLeadForEditInfo updateCompanyName(String data) {
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(data);
		return this;
	}

	public updateViewLeadPage clickUpdateButton() {
		driver.findElementByName("submitButton").click();
		return new updateViewLeadPage();
	}
	
	
	
	

}
