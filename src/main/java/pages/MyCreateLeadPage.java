package pages;

import wrappers.Annotations;

public class MyCreateLeadPage extends Annotations {
	
	public MyCreateLeadPage enterCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}
	public MyCreateLeadPage enterFirstName(String data) {
		
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}
	public MyCreateLeadPage enterLastName(String data) {
		
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}
	public MyViewLeadPage clickCreatLeadButton() {
		driver.findElementByName("submitButton").click();
		return new MyViewLeadPage();
		}

}
